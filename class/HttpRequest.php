<?php


	/**
	 *
	 *   FlaskHTTP
	 *   The HTTP request component
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\FlaskHTTP;


	class HttpRequest
	{


		/**
		 *   cURL handle
		 *   @var resource
		 *   @access public
		 */

		public $CH = null;


		/**
		 *   URL
		 *   @var string
		 *   @access public
		 */

		public $URL = '';


		/**
		 *   Effective URL
		 *   @var string
		 *   @access public
		 */

		public $effectiveURL = '';


		/**
		 *   Method
		 *   @var string
		 *   @access public
		 */

		public $requestMethod = 'GET';


		/**
		 *   Request headers
		 *   @var array
		 *   @access public
		 */

		public $requestHeader = array();


		/**
		 *   Options
		 *   @var array
		 *   @access public
		 */

		public $requestOptions = array();


		/**
		 *   Get fields
		 *   @var array
		 *   @access public
		 */

		public $getFields = array();


		/**
		 *   Post fields
		 *   @var array
		 *   @access public
		 */

		public $postFields = array();


		/**
		 *   File uploads
		 *   @var array
		 *   @access public
		 */

		public $fileUploads = array();


		/**
		 *   Raw POST data
		 *   @var string
		 *   @access public
		 */

		public $rawPostData = null;


		/**
		 *   Multi-part request?
		 *   @var bool
		 *   @access public
		 */

		public $multiPart = false;


		/**
		 *   Multi-part request parts
		 *   @var array
		 *   @access public
		 */

		public $multiPartParts = array();


		/**
		 *   Response info
		 *   @var array
		 *   @access public
		 */

		public $responseInfo = null;


		/**
		 *   Response headers
		 *   @var array
		 *   @access public
		 */

		public $responseHeader = array();


		/**
		 *   Response body
		 *   @var string
		 *   @access public
		 */

		public $responseBody = null;


		/**
		 * 
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param string $URL URL
		 *   @param string $requestMethod Method
		 *   @param array  $requestOptions Options
		 *   @return HttpRequest
		 * 
		 */

		public function __construct( $URL=null, $requestMethod=null, $requestOptions=null )
		{
			// Set parameters
			if (!empty($URL)) $this->setURL($URL);
			if (!empty($requestMethod)) $this->setRequestMethod($requestMethod);
			if (!empty($requestOptions)) $this->setOptions($requestOptions);
		}


		/**
		 * 
		 *   Set URL
		 *   -------
		 *   @access public
		 *   @param string $URL URL
		 *   @return HttpRequest
		 * 
		 */

		public function setURL( string $URL )
		{
			$this->URL=$URL;
			return $this;
		}


		/**
		 *
		 *   Set method
		 *   ----------
		 *   @access public
		 *   @param string $requestMethod Request method (POST, GET, PUT, PATCH, DELETE)
		 *   @return HttpRequest
		 *
		 */

		public function setRequestMethod( $requestMethod )
		{
			$this->requestMethod=mb_strtoupper($requestMethod);
			return $this;
		}


		/**
		 *
		 *   Set request option
		 *   ------------------
		 *   @access public
		 *   @param string $optionName Option name
		 *   @param mixed $optionValue Option value
		 *   @return HttpRequest
		 *
		 */

		public function setOption( $optionName, $optionValue )
		{
			$this->requestOptions[$optionName]=$optionValue;
			return $this;
		}


		/**
		 *
		 *   Set request options
		 *   -------------------
		 *   @access public
		 *   @param array $requestOptions Options
		 *   @return HttpRequest
		 *
		 */

		public function setOptions( $requestOptions )
		{
			foreach ($requestOptions as $k => $v)
			{
				$this->requestOptions[$k]=$v;
			}
			return $this;
		}


		/**
		 *
		 *   Set GET field
		 *   -------------
		 *   @access public
		 *   @param string $field Field name
		 *   @param mixed $value Value
		 *   @return HttpRequest
		 *
		 */

		public function setGetField( $field, $value )
		{
			$this->getFields[$field]=$value;
			return $this;
		}


		/**
		 *
		 *   Set GET fields
		 *   --------------
		 *   @access public
		 *   @param array $getFields GET fields
		 *   @return HttpRequest
		 *
		 */

		public function setGetFields( $getFields )
		{
			foreach ($getFields as $k => $v)
			{
				$this->getFields[$k]=$v;
			}
			return $this;
		}


		/**
		 *
		 *   Set POST field
		 *   --------------
		 *   @access public
		 *   @param string $field Field name
		 *   @param mixed $value Value
		 *   @return HttpRequest
		 *
		 */

		public function setPostField( $field, $value )
		{
			$this->postFields[$field]=$value;
			return $this;
		}


		/**
		 *
		 *   Set POST fields
		 *   ---------------
		 *   @access public
		 *   @param array $postFields Post fields
		 *   @return HttpRequest
		 *
		 */

		public function setPostFields( $postFields )
		{
			foreach ($postFields as $k => $v)
			{
				$this->postFields[$k]=$v;
			}
			return $this;
		}


		/**
		 *
		 *   Set raw POST data
		 *   -----------------
		 *   @access public
		 *   @param string $rawPostData Raw POST data
		 *   @return HttpRequest
		 *
		 */

		public function setRawPostData( $rawPostData )
		{
			$this->rawPostData=$rawPostData;
			return $this;
		}


		/**
		 *
		 *   Set up file upload
		 *   ------------------
		 *   @access public
		 *   @param string $sourceFilename Source filename
		 *   @param string $contentType Content-type
		 *   @param string $postFilename POST file name
		 *   @param string $postFileID Post file ID
		 *   @throws \Exception
		 *   @return HttpRequest
		 *
		 */

		public function setFileUpload( $sourceFilename, $contentType=null, $postFilename=null, $postFileID=null )
		{
			// Check & init
			if (!strlen($sourceFilename)) throw new HttpRequestException('Missing source file name.');
			if (!is_readable($sourceFilename)) throw new HttpRequestException('Source file not readable.');

			// Check/get filename
			if (empty($postFilename))
			{
				$postFilename=pathinfo($sourceFilename,PATHINFO_BASENAME);
			}

			// Check/get content type
			if (empty($contentType))
			{
				$contentType=static::getMimeType($postFilename);
			}

			// File ID
			if (empty($postFileID))
			{
				$postFileID=uniqid();
			}

			// Create CURL file object
			$cfile=curl_file_create($sourceFilename,$contentType,$postFilename);

			// Set method, just-in-case
			$this->setRequestMethod('POST');

			// Add to file upload list
			$this->fileUploads[$postFileID]=$cfile;

			// Return self
			return $this;
		}


		/**
		 *
		 *   Set multi-part request
		 *   ----------------------
		 *   @access public
		 *   @param string $rawPostData Raw POST data
		 *   @return HttpRequest
		 *
		 */

		public function setMultiPart( $multiPart )
		{
			$this->multiPart=(!empty($multiPart)?true:false);
			return $this;
		}


		/**
		 *
		 *   Add a multi-part part
		 *   ---------------------
		 *   @access public
		 *   @param object $part Part
		 *   @throws HttpRequestException
		 *   @return HttpRequest
		 *
		 */

		public function addPart( $multiPartPart )
		{
			if (empty($multiPartPart)) throw new HttpRequestException('Missing part.');
			if (!($multiPartPart instanceof HttpRequestPart)) throw new HttpRequestException('Part must be a HttpRequestPart instance.');
			$this->multiPartParts[]=$multiPartPart;
			return $this;
		}


		/**
		 *
		 *   Set login
		 *   ---------
		 *   @access public
		 *   @param string $username Username
		 *   @param string $password Password
		 *   @return HttpRequest
		 *
		 */

		public function setLogin( $username, $password )
		{
			$this->setOption('httpauth',$username.':'.$password);
			return $this;
		}


		/**
		 *
		 *   Set certificate-based login
		 *   ---------------------------
		 *   @access public
		 *   @param string $certificate Certificate file
		 *   @param string $key Key file
		 *   @param string $certificatePassword Certificate file password
		 *   @param string $keyPassword Key file password
		 *   @param string $certificateType Certificate type
		 *   @return HttpRequest
		 *
		 */

		public function setCertificateLogin( $certificate, $key=null, $certificatePassword=null, $keyPassword=null, $certificateType=null )
		{
			$this->setOption('ssl_cert',$certificate);
			if ($key!==null) $this->setOption('ssl_key',$key);
			if ($certificatePassword!==null) $this->setOption('ssl_certpasswd',$certificatePassword);
			if ($keyPassword!==null) $this->setOption('ssl_keypasswd',$keyPassword);
			if ($certificateType!==null) $this->setOption('ssl_keytype',$certificateType);
			return $this;
		}


		/**
		 *
		 *   Set request header
		 *   ------------------
		 *   @access public
		 *   @param string $headerName Header name
		 *   @param mixed $headerValue Header value
		 *   @return HttpRequest
		 *
		 */

		public function setRequestHeader( $headerName, $headerValue )
		{
			$this->requestHeader[$headerName]=$headerValue;
			return $this;
		}


		/**
		 *
		 *   Set request options
		 *   -------------------
		 *   @access public
		 *   @param array $requestHeaders Headers
		 *   @return HttpRequest
		 *
		 */

		public function setRequestHeaders( $requestHeaders )
		{
			foreach ($requestHeaders as $k => $v)
			{
				$this->requestHeader[$k]=$v;
			}
			return $this;
		}


		/**
		 *
		 *   Set user agent
		 *   --------------
		 *   @access public
		 *   @param string $userAgent User agent string
		 *   @return HttpRequest
		 *
		 */

		public function setUserAgent( $userAgent )
		{
			$this->setRequestHeader('User-Agent',$userAgent);
			return $this;
		}


		/**
		 *
		 *   Set request content-type
		 *   ------------------------
		 *   @access public
		 *   @param string $requestContentType Request content-type
		 *   @return HttpRequest
		 *
		 */

		public function setRequestContentType( $requestContentType )
		{
			$this->setRequestHeader('Content-Type',$requestContentType);
			return $this;
		}


		/**
		 *
		 *   Build a HTTP GET query string from an array
		 *   -------------------------------------------
		 *   @access public
		 *   @static
		 *   @param array $fields Fields
		 *   @param bool $fixSpace Fix spaces
		 *   @param string $encodeTo Encode result to given charset
		 *   @return string
		 *
		 */

		public static function buildQueryString( $fields, $fixSpace=false, $encodeTo=null )
		{
			$QS=array();
			foreach ($fields as $k => $v)
			{
				if ($encodeTo) $v=iconv('UTF-8',$encodeTo,$v);
				$v=urlencode($v);
				if ($fixSpace) $v=str_replace('+','%20',$v);
				if ($fixSpace) $v=str_replace('%3B',';',$v);
				$QS[]=$k.'='.$v;
			}
			return join('&',$QS);
		}


		/**
		 *
		 *   Trigger: pre-request
		 *   --------------------
		 *   @access public
		 *   @throws \Exception
		 *   @return void
		 *
		 */

		public function triggerPreRequest()
		{
			// This trigger function can be implemented in the subclass if necessary.
		}


		/**
		 *
		 *   Trigger: successful request
		 *   ---------------------------
		 *   @access public
		 *   @throws \Exception
		 *   @return void
		 *
		 */

		public function triggerRequestSuccess()
		{
			// This trigger function can be implemented in the subclass if necessary.
		}


		/**
		 *
		 *   Trigger: failed request
		 *   -----------------------
		 *   @access public
		 *   @param int $errorNo Error number
		 *   @param string $errorMessage Error message
		 *   @throws \Exception
		 *   @return void
		 *
		 */

		public function triggerRequestFail( int $errorNo=0, string $errorMessage='' )
		{
			// This trigger function can be implemented in the subclass if necessary.
		}


		/**
		 *
		 *   Make request
		 *   ------------
		 *   @access public
		 *   @throws \Exception
		 *   @return string content
		 *
		 */

		public function send()
		{
			try
			{
				// Check
				if (!mb_strlen($this->URL)) throw new HttpRequestException('URL not specified.');
				if (!in_array($this->requestMethod,array('GET','POST','PUT','PATCH','DELETE'))) throw new HttpRequestException('Invalid request method.');

				// Init
				$this->CH=curl_init();
				if (empty($this->CH)) throw new HttpRequestException('Error initializing curl.');

				// Set URL
				$this->effectiveURL=$this->URL;
				if (sizeof($this->getFields))
				{
					$this->effectiveURL.='?'.static::buildQueryString($this->getFields);
				}
				curl_setopt($this->CH,CURLOPT_URL,$this->effectiveURL);

				// Set method
				switch ($this->requestMethod)
				{
					case 'GET':
						curl_setopt($this->CH,CURLOPT_HTTPGET,1);
						break;
					case 'POST':
					case 'PUT':
					case 'PATCH':
					case 'DELETE':
						curl_setopt($this->CH,CURLOPT_POST,1);
						if ($this->requestMethod!='POST')
						{
							curl_setopt($this->CH, CURLOPT_CUSTOMREQUEST, $this->requestMethod);
						}
						if ($this->multiPart)
						{
							if (!sizeof($this->multiPartParts)) throw new HttpRequestException('Multi-part request, but no parts defined.');
							$boundary=uniqid();
							$delimiter='-------------'.$boundary;
							$this->setRequestContentType('multipart/form-data; boundary='.$boundary);
							$requestBody='';
							foreach ($this->multiPartParts as $Part)
							{
								$requestBody.=$Part->getPart($boundary);
							}
							$requestBody.='--'.$delimiter.'--'."\r\n";
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$requestBody);
						}
						if (!empty($this->fileUploads))
						{
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$this->fileUploads);
						}
						elseif (mb_strlen($this->rawPostData))
						{
							if (empty($this->requestHeader['Content-Type'])) $this->requestHeader['Content-Type']='text/plain';
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$this->rawPostData);
						}
						else
						{
							curl_setopt($this->CH,CURLOPT_POSTFIELDS,$this->postFields);
						}
						break;
					default:
						throw new HttpRequestException('Unknown request method: '.$this->requestMethod);
				}

				// Apply options
				if (array_key_exists('httpauth',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_HTTPAUTH,CURLAUTH_BASIC) ;
					curl_setopt($this->CH,CURLOPT_USERPWD,$this->requestOptions['httpauth']);
				}
				elseif (array_key_exists('username',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_HTTPAUTH,CURLAUTH_BASIC) ;
					curl_setopt($this->CH,CURLOPT_USERPWD,$this->requestOptions['username'].":".$this->requestOptions['password']);
				}
				if (array_key_exists('ssl_cert',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_SSLCERT,$this->requestOptions['ssl_cert']);
					if (array_key_exists('ssl_certpasswd',$this->requestOptions))
					{
						curl_setopt($this->CH,CURLOPT_SSLCERTPASSWD,$this->requestOptions['ssl_certpasswd']);
					}
					if (array_key_exists('ssl_key',$this->requestOptions))
					{
						curl_setopt($this->CH,CURLOPT_SSLKEY,$this->requestOptions['ssl_key']);
					}
					if (array_key_exists('ssl_keypasswd',$this->requestOptions))
					{
						curl_setopt($this->CH,CURLOPT_SSLKEYPASSWD,$this->requestOptions['ssl_keypasswd']);
					}
					if (array_key_exists('ssl_keytype',$this->requestOptions))
					{
						curl_setopt($this->CH,CURLOPT_SSLCERTTYPE,$this->requestOptions['ssl_keytype']);
						curl_setopt($this->CH,CURLOPT_SSLKEYTYPE,$this->requestOptions['ssl_keytype']);
					}
				}
				if (array_key_exists('ssl_verifyhost',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_SSL_VERIFYHOST,$this->requestOptions['ssl_verifyhost']);
				}
				else
				{
					curl_setopt($this->CH,CURLOPT_SSL_VERIFYHOST,false);
				}
				if (array_key_exists('ssl_verifypeer',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_SSL_VERIFYPEER,$this->requestOptions['ssl_verifypeer']);
				}
				else
				{
					curl_setopt($this->CH,CURLOPT_SSL_VERIFYPEER,false);
				}
				if (array_key_exists('ssl_cainfo',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_CAINFO,$this->requestOptions['ssl_cainfo']);
				}
				else
				{
					curl_setopt($this->CH, CURLOPT_CAINFO,__DIR__.'/../cacert/cacert.pem');
				}
				if (array_key_exists('ssl_version',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_SSLVERSION,$this->requestOptions['ssl_version']);
				}
				if (array_key_exists('followlocation',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_FOLLOWLOCATION,$this->requestOptions['followlocation']);
				}
				else
				{
					curl_setopt($this->CH,CURLOPT_FOLLOWLOCATION,true);
				}
				if (array_key_exists('connecttimeout',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_CONNECTTIMEOUT,$this->requestOptions['connecttimeout']);
				}
				else
				{
					curl_setopt($this->CH,CURLOPT_CONNECTTIMEOUT,'15');
				}
				if (array_key_exists('timeout',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_TIMEOUT,$this->requestOptions['timeout']);
				}
				else
				{
					curl_setopt($this->CH,CURLOPT_TIMEOUT,'60');
				}
				if (array_key_exists('cookiejar',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_COOKIEJAR,$this->requestOptions['cookiejar']);
					curl_setopt($this->CH,CURLOPT_COOKIEFILE,$this->requestOptions['cookiejar']);
				}
				elseif (!empty($this->requestOptions['newcookiesession']))
				{
					curl_setopt($this->CH,CURLOPT_COOKIESESSION,true);
				}
				if (array_key_exists('referer',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_REFERER,$this->requestOptions['referer']);
				}
				if (array_key_exists('useragent',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_USERAGENT,$this->requestOptions['useragent']);
				}
				if (array_key_exists('interface',$this->requestOptions))
				{
					curl_setopt($this->CH,CURLOPT_INTERFACE,$this->requestOptions['interface']);
				}

				// Apply headers
				if (sizeof($this->requestHeader))
				{
					$requestHeader=array();
					foreach ($this->requestHeader as $header => $value)
					{
						$requestHeader[]=$header.': '.$value;
					}
					curl_setopt($this->CH,CURLOPT_HTTPHEADER,$requestHeader);
				}

				// Those shall we always need
				curl_setopt($this->CH,CURLOPT_ENCODING,'');
				curl_setopt($this->CH,CURLOPT_HEADER,1);
				curl_setopt($this->CH,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($this->CH,CURLINFO_HEADER_OUT,1);

				// Run pre-request trigger
				$this->triggerPreRequest();

				// Exec
				$response=curl_exec($this->CH);

				// Error?
				if ($response===false)
				{
					$errorNo=curl_errno($this->CH);
					$errorMessage=curl_error($this->CH);
					$this->triggerRequestFail($errorNo,$errorMessage);
					throw new HttpRequestException('Error making HTTP request: '.$errorNo.' - '.$errorMessage);
				}

				// Store request info
				$this->responseInfo=curl_getinfo($this->CH);

				// Split header and body
				$responseHeader=substr($response,0, $this->responseInfo['header_size']);
				$this->responseBody=substr($response,$this->responseInfo['header_size']);

				// Split headers
				$this->responseHeader=array_map(
					function($x){
						return array_map("trim",explode(":", $x,2));
					},
					array_filter(
						array_map("trim",explode("\n",$responseHeader))
					)
				);

				// Close handle
				curl_close($this->CH);

				// Run post-request trigger
				$this->triggerRequestSuccess();

				// Return
				return $this->responseBody;
			}
			catch (\Exception $e)
			{
				if (!empty($this->CH)) curl_close($this->CH);
				throw $e;
			}
		}


		/**
		 *
		 *   Get HTTP response info
		 *   ----------------------
		 *   @access public
		 *   @return array
		 *
		 */

		public function getResponseInfo()
		{
			return $this->responseInfo;
		}


		/**
		 *
		 *   Get HTTP response code
		 *   ----------------------
		 *   @access public
		 *   @return int
		 *
		 */

		public function getResponseCode()
		{
			return (array_key_exists('http_code',$this->responseInfo)?$this->responseInfo['http_code']:null);
		}


		/**
		 *
		 *   Get response headers
		 *   --------------------
		 *   @access public
		 *   @return array
		 *
		 */

		public function getResponseHeaders()
		{
			return $this->responseHeader;
		}


		/**
		 *
		 *   Get response header
		 *   -------------------
		 *   @access public
		 *   @param string $headerName Header name
		 *   @param bool $caseSensitive Is match case sensitive?
		 *   @return string|array
		 *
		 */

		public function getResponseHeader( string $headerName, bool $caseSensitive=false )
		{
			$responseHeader=array();
			foreach ($this->responseHeader as $header)
			{
				if ($header[0]==$headerName || (!$caseSensitive && mb_strtolower($header[0])==mb_strtolower($headerName)))
				{
					$responseHeader[]=$header[1];
				}
			}
			if (sizeof($responseHeader)>1)
			{
				return $responseHeader;
			}
			elseif (sizeof($responseHeader)==1)
			{
				return $responseHeader[0];
			}
			else
			{
				return null;
			}
		}


		/**
		 *
		 *   Get MIME type for filename
		 *   --------------------------
		 *   @access public
		 *   @static
		 *   @param string $filename Filename
		 *   @throws \Exception
		 *   @return string|bool
		 *
		 */

		public static function getMimeType( $filename )
		{
			// Get file extension
			$fileExtension=mb_strtolower(pathinfo($filename,PATHINFO_EXTENSION));

			// Read file
			$mimeTypes=file(__DIR__.'/../mime/mime.types',FILE_IGNORE_NEW_LINES);
			if (empty($mimeTypes) || !is_array($mimeTypes)) throw new HttpRequestException('Error reading mime.types file.');

			// Find MIME type
			foreach ($mimeTypes as $line)
			{
				if (empty($line) || $line[0]=='#') continue;
				$lineArr=preg_split("/\s+/",$line);
				if (empty($lineArr[1])) continue;
				for ($i=1;$i<sizeof($lineArr);++$i)
				{
					if (mb_strtolower($lineArr[$i])==$fileExtension) return $lineArr[0];
				}
			}

			// Nothing?
			return false;
		}


	}


?>