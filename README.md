# FlaskHTTP - a neat and tidy HTTP request component by Codelab

FlaskHTTP is a simple HTTP request component to complement the [FlaskPHP](https://bitbucket.org/codelab-solutions/flaskphp) framework. 

We created it as a separate component in order to use it also for VoogPHP, the PHP API for the VOOG CMS platform.
